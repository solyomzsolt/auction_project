#ifndef REGULARINTERVALTIMER_H
#define REGULARINTERVALTIMER_H

#include "regularintervaltimerimp.h"
#include "RegularIntervalTimerCallback.h"

class RegularIntervalTimer {
public:
  RegularIntervalTimer(RegularIntervalTimerImp *);
  ~RegularIntervalTimer() = default;
  void setInterval(int);
  void startTimer();
  void stopTimer();
  void setCallback(RegularIntervalTimerCallback* );
private:
  RegularIntervalTimerImp *m_timerImp;
};

#endif // REGULARINTERVALTIMER_H
