#ifndef REGULARINTERVALTIMERIMP_H
#define REGULARINTERVALTIMERIMP_H

#include "RegularIntervalTimerCallback.h"

class RegularIntervalTimerImp {
public:
  RegularIntervalTimerImp() = default;
  virtual void setInterval(int) = 0;
  virtual void setCallback(RegularIntervalTimerCallback* ) = 0;
  virtual void startTimer() = 0;
  virtual void stopTimer() = 0;
  ~RegularIntervalTimerImp()= default;
};

#endif // REGULARINTERVALTIMERIMP_Hs
