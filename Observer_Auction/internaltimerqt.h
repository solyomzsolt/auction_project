#ifndef INTERNALTIMERQT_H
#define INTERNALTIMERQT_H

#include <QDebug>
#include <QTimer>
#include "RegularIntervalTimerCallback.h"

class InternalTimerQT : public QObject {
  Q_OBJECT
public:
  InternalTimerQT();
  ~InternalTimerQT();
  QTimer *timer1;
  void setCallback(RegularIntervalTimerCallback* );
private slots:
  // what i have to call here, where can I implement this
  void onTimer();
private:
  RegularIntervalTimerCallback* m_callback;
};

#endif // INTERNALTIMERQT_H
