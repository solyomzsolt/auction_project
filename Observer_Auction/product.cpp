#include "product.h"

Product::Product(){};
Product::~Product(){};
Product::Product(std::string name, int price)
    : productName(name), productPrice(price) {
  std::cout << "The product with name: " << productName
            << " was created and the starting price is: " << productPrice
            << std::endl;
};

void Product::setProductPrice(int price) { productPrice = price; }

double Product::getProductPrice() const { return productPrice; }

std::string Product::getPruductName() const { return productName; }
