#ifndef REGULARINTERVALTIMERIMPQT_H
#define REGULARINTERVALTIMERIMPQT_H

#include "regularintervaltimerimp.h"
#include "internaltimerqt.h"

class RegularIntervalTimerImpQT : public RegularIntervalTimerImp {

public:
  RegularIntervalTimerImpQT();
  ~RegularIntervalTimerImpQT()= default;
  void stopTimer() override;
  void startTimer() override;
  void setCallback(RegularIntervalTimerCallback* ) override; //ask about this
  void setInterval(int) override;



protected:
  InternalTimerQT* m_qtTimer;
  int m_interval;
};

#endif // REGULARINTERVALTIMERIMPQT_H
