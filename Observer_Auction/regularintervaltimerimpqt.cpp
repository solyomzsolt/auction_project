#include "regularintervaltimerimpqt.h"
#include <iostream>


RegularIntervalTimerImpQT::RegularIntervalTimerImpQT()
{
    m_qtTimer = new InternalTimerQT();
}


void RegularIntervalTimerImpQT ::setInterval(int interval) {
  m_interval = interval;
}

void RegularIntervalTimerImpQT ::startTimer() {
  m_qtTimer->timer1->start(m_interval);
}

void RegularIntervalTimerImpQT ::stopTimer() { m_qtTimer->timer1->stop();

                                             delete m_qtTimer;
                                             }

void RegularIntervalTimerImpQT ::setCallback(RegularIntervalTimerCallback* callback) { m_qtTimer->setCallback(callback); }

