#ifndef REGULARINTERVALTIMERCALLBACK_H
#define REGULARINTERVALTIMERCALLBACK_H

class RegularIntervalTimerCallback {
public:
  RegularIntervalTimerCallback() = default;
  virtual ~RegularIntervalTimerCallback() = default;
  virtual void onTimerTriggered() = 0;
};
#endif // REGULARINTERVALTIMERCALLBACK_H
