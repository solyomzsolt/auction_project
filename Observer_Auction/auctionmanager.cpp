#include "auctionmanager.h"

AuctionManager::AuctionManager() {}
AuctionManager::~AuctionManager() {}

void AuctionManager::onPriceUpdate(const Participant &person,
                                   const Product &prod) {
  auc_prod = prod;
  // Iterate participants and notify aboud new price
  for (int i = 0; i < auc_part.size(); i++) {
    auc_part[i]->onAuctionPriceUpdated(auc_prod);
  }

  //here we decide the actual winner
  int winner_index = 0;
  for (int i = 1; i < auc_part.size(); i++) {
    if (auc_part[i]->getPriceOffered() >
        auc_part[winner_index]->getPriceOffered()) {
      winner_index = i;
    }
  }

  //if we have two participants with the same price offered we don't have winner
  bool single_winner = true;
  for (int i = 1; i < auc_part.size(); i++) {
    if (winner_index != i && auc_part[i]->getPriceOffered() >=
                                 auc_part[winner_index]->getPriceOffered()) {
      single_winner = false;
    }
  }

  //if we have only one participant with the highest price he is the winner
  if (single_winner) {
    winner = auc_part[winner_index];
  } else {
    winner = 0;
  }

}

//participants who are particip at the auction
void AuctionManager::addParticipant(Participant *partptr) {
  auc_part.push_back(partptr);
  // we dont have to erase it to av
}

Participant *AuctionManager::getCurrentWinner() { return winner; }
