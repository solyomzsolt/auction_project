#include "participant.h"
#include "product.h"

Participant::Participant() { priceOffered = 0; }

Participant::Participant(std::string name)
    : participantName(name), priceOffered(0) {
  std::cout << "The participant with name was added to participants vector: " << participantName << std::endl;
}

int Participant::getPriceOffered() { return priceOffered; }

void Participant::setPriceOffered(int price) { priceOffered = price; }

std::string Participant::getParticipantName() { return participantName; }

void Participant::onAuctionPriceUpdated(const Product &prod) {
  // Decide if product price is higher and enable the flag

  productPrice = prod.getProductPrice();
  isCurrentBidHigher = currentBid > productPrice;

  if (prod.getProductPrice() > priceOffered) {
    isPriceHigher = true;
  } else {
    isPriceHigher = false;
  }
}

bool Participant::canRaisePrice() {
  return isPriceHigher || isCurrentBidHigher;
}

void Participant::setCurrentBid(int b) {
  currentBid = b;
  isCurrentBidHigher = currentBid > productPrice;
}

Participant::~Participant() {}
