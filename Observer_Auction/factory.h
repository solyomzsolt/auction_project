#ifndef FACTORY_H
#define FACTORY_H

#include "regularintervaltimerimp.h"

class Factory
{
public:

    Factory();
   ~Factory();
    RegularIntervalTimerImp* createTimer();
private:
    RegularIntervalTimerImp* createdTimer;

};

#endif // FACTORY_H
