#ifndef AUCTIONMANAGER_H
#define AUCTIONMANAGER_H
#include <iostream>
#include <vector>
#include <participant.h>
#include <product.h>

class AuctionManager
{
public:
    AuctionManager();
    ~AuctionManager();
    void onPriceUpdate(const Participant&, const Product&);
    void addParticipant(Participant* partptr);
    Participant* getCurrentWinner();
private:
    std::vector<Participant*> auc_part;
    Product auc_prod;
    Participant* winner = 0;
};

#endif // AUCTIONMANAGER_H
