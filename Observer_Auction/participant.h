#ifndef PARTICIPANTS_H
#define PARTICIPANTS_H

#include <auctionmanagerlistener.h>
#include <iostream>

class Participant : public AuctionManagerListener {
public:
  Participant();
  Participant(std::string);

  virtual ~Participant() override;
  int getPriceOffered();
  void setPriceOffered(int);
  void setCurrentBid(int);
  bool canRaisePrice();

  std::string getParticipantName();

  void onAuctionPriceUpdated(const Product &prod) override;

private:
  std::string participantName;
  int priceOffered = 0;
  bool isPriceHigher = false;
  bool isCurrentBidHigher = false;
  int currentBid = 0;
  int productPrice = 0;
};

#endif // PARTICIPANTS_H
