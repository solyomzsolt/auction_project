#include "regularintervaltimer.h"

RegularIntervalTimer::RegularIntervalTimer(RegularIntervalTimerImp *timerimp)
    : m_timerImp(timerimp) {}

void RegularIntervalTimer ::setInterval(int ms) { m_timerImp->setInterval(ms); }

void RegularIntervalTimer::startTimer() { m_timerImp->startTimer(); }

void RegularIntervalTimer::stopTimer() { m_timerImp->stopTimer(); }

void RegularIntervalTimer::setCallback(RegularIntervalTimerCallback *callback) {
  m_timerImp->setCallback(callback);
}
