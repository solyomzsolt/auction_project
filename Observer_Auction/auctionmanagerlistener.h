#ifndef AUCTIONMANAGERLISTENER_H
#define AUCTIONMANAGERLISTENER_H
#include "product.h"

class AuctionManagerListener {
public:
  AuctionManagerListener() = default;
  virtual ~AuctionManagerListener() = default;
  virtual void onAuctionPriceUpdated(const Product &prod) = 0;
};

#endif // AUCTIONMANAGERLISTENER_H
