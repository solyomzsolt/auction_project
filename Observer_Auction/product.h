#ifndef PRODUCT_H
#define PRODUCT_H
#include <iostream>

class Product {
public:
  Product();
  Product(std::string, int);
  void setProductPrice(int);
  double getProductPrice() const;
  std::string getPruductName() const;
  ~Product();

private:
  std::string productName;
  int productPrice;
};

#endif // PRODUCT_H
