#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <vector>

#include "auctionmanager.h"
#include "factory.h"
#include "participant.h"
#include "product.h"
#include "regularintervaltimer.h"
#include <QDebug>

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow, public RegularIntervalTimerCallback{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  void updateUI();

private slots:
  void on_pushButton_clicked();

  void on_pushButton_2_clicked();

  void on_pushButton_3_clicked();

  void on_doubleSpinBox_valueChanged(double arg1);

  void on_doubleSpinBox_2_valueChanged(double arg1);

  void on_doubleSpinBox_3_valueChanged(double arg1);

  void on_pushButton_7_clicked();

  void on_doubleSpinBox_4_valueChanged(double arg1);

  void on_pushButton_4_clicked();

  void on_pushButton_5_clicked();

  void onTimerTriggered() override;


private:
  Ui::MainWindow *ui;
  std::vector<Participant *> particip;
  std::vector<Product *> product;
  AuctionManager manager;
  RegularIntervalTimer *timer;
  Factory *f1;
  Product *p1;
  int timeleft = 20;
  RegularIntervalTimerImp* created;
};

#endif // MAINWINDOW_H
