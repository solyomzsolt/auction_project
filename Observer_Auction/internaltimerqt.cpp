#include "internaltimerqt.h"

InternalTimerQT::InternalTimerQT() : m_callback(NULL) {

  timer1 = new QTimer();

  connect(timer1, SIGNAL(timeout()), this, SLOT(onTimer()));
}

InternalTimerQT::~InternalTimerQT()
{
    delete timer1;
}

void InternalTimerQT ::setCallback(RegularIntervalTimerCallback *callback) {
  m_callback = callback;
   qDebug() << "callback set";
}

void InternalTimerQT::onTimer() {
    //every second this function will be called
  if (m_callback) {
    m_callback->onTimerTriggered();
  }
  qDebug() << "timer triggered";
}
