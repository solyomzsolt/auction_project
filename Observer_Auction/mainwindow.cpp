#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {

  ui->setupUi(this);

  // trying to add background to my mainwindow
  QPixmap bkgnd("C:/Users/solyo/Pictures/auction_texture.jpg");
  bkgnd = bkgnd.scaled(this->size());
  QPalette palette;
  palette.setBrush(QPalette::Background, bkgnd);
  this->setPalette(palette);

 p1 = new Product("Samsung Super Amoled 32inch", 3000);
 f1 = new Factory();
 // create a new RegularIntervalTimer with using factory
 timer = new RegularIntervalTimer(f1->createTimer());

  QString qName = QString::fromStdString(p1->getPruductName());

  ui->lcdNumber_2->display(
      timeleft); // here i setted the timeleft screen to standard

  product.push_back(p1);

  ui->textBrowser->setText(qName);

  ui->lcdNumber->display(product[0]->getProductPrice());

  ui->pushButton->setEnabled(false);
  ui->pushButton_2->setEnabled(false);
  ui->pushButton_3->setEnabled(false);
  ui->pushButton_4->setEnabled(false);

  ui->doubleSpinBox->setEnabled(false);
  ui->doubleSpinBox_2->setEnabled(false);
  ui->doubleSpinBox_3->setEnabled(false);
  ui->doubleSpinBox_4->setEnabled(false);
}

void MainWindow::updateUI() {

  if ((particip[0]->canRaisePrice()) &&
      (particip[0]->getParticipantName() != "")) {
    ui->pushButton->setEnabled(particip[0]->canRaisePrice());
  }
  if ((particip[1]->canRaisePrice()) &&
      (particip[1]->getParticipantName() != "")) {
    ui->pushButton_2->setEnabled(particip[1]->canRaisePrice());
  }
  if ((particip[2]->canRaisePrice()) &&
      (particip[2]->getParticipantName() != "")) {
    ui->pushButton_3->setEnabled(particip[2]->canRaisePrice());
  }

  if ((particip[3]->canRaisePrice()) &&
      (particip[3]->getParticipantName() != "")) {
    ui->pushButton_4->setEnabled(particip[3]->canRaisePrice());
  }

  ui->lcdNumber->display(product[0]->getProductPrice());
}

void MainWindow::on_pushButton_clicked() { // first accept button first person
  ui->doubleSpinBox->setSingleStep(100);

  if (ui->doubleSpinBox->value() < product[0]->getProductPrice()) {
    double price = product[0]->getProductPrice();
    ui->doubleSpinBox->setRange(product[0]->getProductPrice(), 100000000);
    ui->doubleSpinBox->setValue(price);
  }

  int value1 = ui->doubleSpinBox->value();
  product[0]->setProductPrice(value1);
  particip[0]->setPriceOffered(value1);

  manager.onPriceUpdate(*particip[0], *product[0]);
  updateUI();

  std::cout << value1 << std::endl;
}

void MainWindow::on_pushButton_2_clicked() { // second accept button second
                                             // person

  if (ui->doubleSpinBox_2->value() < product[0]->getProductPrice()) {
    ui->doubleSpinBox_2->setValue(product[0]->getProductPrice());
    ui->doubleSpinBox_2->setRange(product[0]->getProductPrice(), 100000000);
  }

  int value2 = ui->doubleSpinBox_2->value();

  product[0]->setProductPrice(value2);
  ui->doubleSpinBox_2->setSingleStep(100);

  particip[1]->setPriceOffered(value2);
  manager.onPriceUpdate(*particip[1], *product[0]);
  updateUI();

  std::cout << value2 << std::endl;
}

void MainWindow::on_pushButton_3_clicked() {

  if (ui->doubleSpinBox_3->value() < product[0]->getProductPrice()) {
    ui->doubleSpinBox_3->setValue(product[0]->getProductPrice());
    ui->doubleSpinBox_3->setRange(product[0]->getProductPrice(), 100000000);
  }

  int value3 = ui->doubleSpinBox_3->value();
  product[0]->setProductPrice(value3);

  ui->doubleSpinBox_3->setSingleStep(100);

  particip[2]->setPriceOffered(value3);

  manager.onPriceUpdate(*particip[2], *product[0]);

  updateUI();
  std::cout << value3 << std::endl;
}

void MainWindow::on_pushButton_4_clicked() {

  if (ui->doubleSpinBox_4->value() < product[0]->getProductPrice()) {
    ui->doubleSpinBox_4->setValue(product[0]->getProductPrice());
    ui->doubleSpinBox_4->setRange(product[0]->getProductPrice(), 100000000);
  }

  int value4 = ui->doubleSpinBox_4->value();
  product[0]->setProductPrice(value4);

  ui->doubleSpinBox_4->setSingleStep(100);

  particip[3]->setPriceOffered(value4);

  manager.onPriceUpdate(*particip[3], *product[0]);

  updateUI();
  std::cout << value4 << std::endl;
}

MainWindow::~MainWindow() {
  delete ui;

  for (int i = 0; i < particip.size(); i++) {
    delete particip[i];
  }
  delete p1;
  delete f1;
  delete timer;

}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1) {
  double value1 = ui->doubleSpinBox->value();
  particip[0]->setCurrentBid(value1);
  updateUI();
}

void MainWindow::on_doubleSpinBox_2_valueChanged(double arg1) {
  double value2 = ui->doubleSpinBox_2->value();
  particip[1]->setCurrentBid(value2);
  updateUI();
}

void MainWindow::on_doubleSpinBox_3_valueChanged(double arg1) {
  double value3 = ui->doubleSpinBox_3->value();
  particip[2]->setCurrentBid(value3);
  updateUI();
}

void MainWindow::on_doubleSpinBox_4_valueChanged(double arg1) {
  double value4 = ui->doubleSpinBox_4->value();
  particip[3]->setCurrentBid(value4);
  updateUI();
}

void MainWindow::on_pushButton_7_clicked() {

  QString qpersName = ui->textEdit->toPlainText();

  QString qpersName1 = ui->textEdit_2->toPlainText();

  if (qpersName != "" && qpersName1 != "") {

    std::string current_locale_text = qpersName.toLocal8Bit().constData();
    Participant *part1 = new Participant(current_locale_text);

    particip.push_back(part1);
    manager.addParticipant(part1);

    if (qpersName != "") {

      ui->pushButton->setEnabled(true);
      ui->doubleSpinBox->setEnabled(true);

    } else {

      ui->pushButton->setEnabled(false);
      ui->doubleSpinBox->setEnabled(false);
    }

    std::string current_locale_text2 = qpersName1.toLocal8Bit().constData();
    Participant *part2 = new Participant(current_locale_text2);

    particip.push_back(part2);
    manager.addParticipant(part2);

    // display participants names
    ui->textBrowser_6->setText("Participants: " + qpersName + ", " +
                               qpersName1 + " were added");

    if (qpersName1 != "") {

      ui->pushButton_2->setEnabled(true);
      ui->doubleSpinBox_2->setEnabled(true);

    } else {

      ui->pushButton_2->setEnabled(false);
      ui->doubleSpinBox_2->setEnabled(false);
    }

    QString qpersName2 = ui->textEdit_3->toPlainText();

    std::string current_locale_text3 = qpersName2.toLocal8Bit().constData();
    Participant *part3 = new Participant(current_locale_text3);

    particip.push_back(part3);
    manager.addParticipant(part3);

    if (qpersName2 != "") {

      ui->textBrowser_6->setText("Participants: " + qpersName + ", " +
                                 qpersName1 + ", " + qpersName2 +
                                 " were added");
      ui->pushButton_3->setEnabled(true);
      ui->doubleSpinBox_3->setEnabled(true);

    } else {

      ui->pushButton_3->setEnabled(false);
      ui->doubleSpinBox_3->setEnabled(false);
    }

    QString qpersName3 = ui->textEdit_4->toPlainText();

    std::string current_locale_text4 = qpersName3.toLocal8Bit().constData();
    Participant *part4 = new Participant(current_locale_text4);

    particip.push_back(part4);
    manager.addParticipant(part4);

    if (qpersName3 != "") {
      ui->textBrowser_6->setText("Participants: " + qpersName + ", " +
                                 qpersName1 + ", " + qpersName2 + ", " +
                                 qpersName3 + " were added");
      ui->pushButton_4->setEnabled(true);
      ui->doubleSpinBox_4->setEnabled(true);

    } else {
      ui->pushButton_4->setEnabled(false);
      ui->doubleSpinBox_4->setEnabled(false);
    }
  } else {
    ui->textBrowser_6->setText("Please add minim two participants");
  }
  for(int i =0 ; i < particip.size(); i++){
      if(i < 2){
          ui->pushButton_7->setEnabled(false);
      }
  }
}

void MainWindow::on_pushButton_5_clicked() {

  QString qpersName = ui->textEdit->toPlainText();
  QString qpersName1 = ui->textEdit_2->toPlainText();
  // this is the start buton, here i will start the auction

  // check if anybody is added to auction
  if (qpersName != "" && qpersName1 != "") {
    timer->setInterval(1000);
    timer->setCallback(this);
    timer->startTimer();
  } else {
    ui->textBrowser_6->setText("Please add minim two participants");
  }
}

void MainWindow::onTimerTriggered() {
  // here i will stop the timer when the time left
  timeleft--;
  ui->lcdNumber_2->display(timeleft);

  if (timeleft == 0) {
    if (manager.getCurrentWinner() != nullptr) {
      QString winnerName = QString::fromStdString(
          manager.getCurrentWinner()->getParticipantName());
      ui->textBrowser_6->setText("The winner is " + winnerName);
      timer->stopTimer();
    } else {
      ui->textBrowser_6->setText("No winner yet, adding extra 5 seconds");
      timeleft = 5;
      ui->lcdNumber_2->display(timeleft);
    }
  }
}
